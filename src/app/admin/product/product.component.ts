import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ProductDetailComponent } from '../product-detail/product-detail.component';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  title:string;
  constructor(
    public dialog:MatDialog
  ) {
    this.title= 'Product';
    
  }

  books:any=[];

  ngOnInit(): void {
    this.getBooks();
  }

  getBooks(){
    this.books=[
      {
        title:'title',
        author:'author',
        pages:300,
      },{
        title:'title2',
        author:'author2',
        pages:400,
      }
    ]
  }

  productDetail(data,idx){

    let dialog = this.dialog.open(ProductDetailComponent,{
      width:'400px',
      data
    });

    dialog.afterClosed().subscribe(res=>{
      if(res){
        if(idx == -1)
          this.books.push(res);
        else
          this.books[idx]=res;
      }
    })

  }

  deleteProduct(idx){

    var conf=confirm('Sure to delete item ?')
    if(conf)
    this.books.splice(idx,1)

  }

}
