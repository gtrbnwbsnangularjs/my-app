import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {

  constructor(
    public dialogref: MatDialogRef<ProductDetailComponent>,
    @Inject(MAT_DIALOG_DATA) public data:any) { 
  }

  ngOnInit(): void {
  }

  saveData(){
    this.dialogref.close(this.data);
  }

}
